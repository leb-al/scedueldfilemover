﻿using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ScedueldFileMover
{
    abstract class Mover
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public string Source { get; set; }
        public string Destination { get; set; }
        public TimeSpan Period { get; set; }

        public virtual void Load(XmlNode node)
        {
            Source = node.Attributes["from"].Value;
            Destination = node.Attributes["to"].Value;
            Period = TimeSpan.Parse(node.Attributes["time"].Value);
            logger.Info("From: '{0}', to: '{1}', time: '{2}'", Source, Destination, Period);
        }

        public abstract DateTime EstimateTime(FileInfo info);

        public virtual void RunMover()
        {
            DateTime now = DateTime.Now;
            DirectoryInfo root = new DirectoryInfo(Source);
            DirectoryInfo[] directories = root.GetDirectories();
            foreach (DirectoryInfo directory in directories)
            {
                MoveDirectory(now, directory);
            }
            FileInfo[] files = root.GetFiles();
            foreach (FileInfo file in files)
            {
                MoveFile(now, file);
            }
        }

        protected void MoveDirectory(DateTime now, DirectoryInfo directory)
        {
            try
            {
                DateTime time = ComputeTime(directory);
                TimeSpan interval = now - time;
                logger.Info("Directory '{0}', estimated time: {1}, interaval: {2}", directory.Name, time, interval);
                if (interval > Period)
                {
                    string destinationPath = Path.Combine(Destination, directory.Name);
                    if (Directory.Exists(destinationPath))
                    {
                        logger.Info("Can't move '{0}' to '{1}'. Directory already exists. Skipped.", directory.FullName, destinationPath);

                    }
                    else
                    {
                        logger.Info("Moving directory '{0}' to {1}", directory.FullName, destinationPath);
                        DirectoryMove(directory.FullName, destinationPath);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("Can't process directory '{0}'. Error: {1}", directory.FullName, ex);
            }
        }

        protected void MoveFile(DateTime now, FileInfo file)
        {
            try
            {
                DateTime time = EstimateTime(file);
                TimeSpan interval = now - time;
                logger.Info("File '{0}', estimated time: {1}, interaval: {2}", file.Name, time, interval);
                if (interval > Period)
                {
                    string destinationPath = Path.Combine(Destination, file.Name);
                    if (File.Exists(destinationPath))
                    {
                        logger.Info("Can't move '{0}' to '{1}'. File already exists.", file.FullName, destinationPath);

                    }
                    else
                    {
                        logger.Info("Moving file '{0}' to {1}", file.FullName, destinationPath);
                        File.Move(file.FullName, destinationPath);
                    }

                }
            }
            catch (Exception ex)
            {
                logger.Error("Can't process file '{0}'. Error: {1}", file.FullName, ex);
            }
        }

        protected static void DirectoryMove(string sourceDirName, string destDirName)
        {
            List<FileSystemInfo> infos = new List<FileSystemInfo>();
            DirectoryCopy(sourceDirName, destDirName, true, infos);
            foreach (var item in infos)
            {
                try
                {
                    item.Attributes = FileAttributes.Normal;
                    item.Delete();
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error deleting '{0}': '{1}", item.FullName, ex.Message);
                }
            }
            Directory.Delete(sourceDirName, true);
        }

        protected static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs, List<FileSystemInfo> infos)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();
            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
                infos.Add(file);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs, infos);
                    infos.Add(subdir);
                }
            }
        }

        protected DateTime ComputeTime(DirectoryInfo directory)
        {
            return directory.GetDirectories().Select(x => ComputeTime(x)).Concat(new DateTime[] { directory.CreationTime }).
                Concat(directory.GetFiles().Select(x => EstimateTime(x))).Max();
        }
    }

    class CreatedMover : Mover
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override DateTime EstimateTime(FileInfo info)
        {
            return info.CreationTime;
        }

        public override void Load(XmlNode node)
        {
            logger.Info("CreatedMover is loading");
            base.Load(node);
        }
    }

    class EditedMover : Mover
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public override DateTime EstimateTime(FileInfo info)
        {
            return info.LastWriteTime;
        }

        public override void Load(XmlNode node)
        {
            logger.Info("EditedMover is loading");
            base.Load(node);
        }
    }
}
