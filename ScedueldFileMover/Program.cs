﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace ScedueldFileMover
{
    static class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            //Logger logger = LogManager.GetCurrentClassLogger();
            logger.Info("Launch program");
            List<string> configs = new List<string>();
            configs.AddRange(Environment.GetCommandLineArgs());
            configs.RemoveAt(0);
            if (configs.Count == 0)
            {
                configs.Add("config.xml");
            }
            List<Mover> movers = new List<Mover>();
            foreach (string configname in configs)
            {
                logger.Info("Config name {0} loading", configname);
                try
                {
                    XmlDocument document = new XmlDocument();
                    document.Load(configname);
                    logger.Debug("Config loaded");
                    if (document.LastChild.Name != "mover")
                    {
                        logger.Fatal("Error procces config '{0}'. Wrong root node name '{1}'", configname, document.LastChild.Name);
                        continue;
                    }
                    foreach (XmlNode node in document.LastChild.ChildNodes)
                    {
                        Mover mover;
                        switch (node.Name)
                        {
                            case "created":
                                mover = new CreatedMover();
                                break;
                            case "edited":
                                mover = new EditedMover();
                                break;
                            default:
                                logger.Error("Unsupported type of element '{1}' in config '{0}'", configname, node.Name);
                                continue;
                        }
                        mover.Load(node);
                        movers.Add(mover);
                    }
                }
                catch (Exception ex)
                {
                    logger.Fatal("Error procces config '{0}'. Exception {1}", configname, ex);
                }
            }
            foreach (Mover mover in movers)
            {
                try
                {
                    mover.RunMover();
                }
                catch (Exception ex)
                {
                    logger.Error("Error executing mover '{0}'. Exception {1}", mover, ex);
                }
            }
            logger.Info("Finish program");
        }
    }
}
